const passwordIcons = document.querySelectorAll('.icon-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');
const submitButton = document.getElementById('submitButton');

passwordIcons.forEach(icon => {
    icon.addEventListener('click', () => {
        const targetInputId = icon.getAttribute('data-toggle');
        const targetInput = document.getElementById(targetInputId);

        if (targetInput.type === 'password') {
            targetInput.type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else {
            targetInput.type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    });
});

submitButton.addEventListener('click', () => {
    const password1 = passwordInputs[0].value;
    const password2 = passwordInputs[1].value;

    if (password1 === password2) {
        alert('You are welcome');
    } else {
        const errorMessage = document.createElement('p');
        errorMessage.textContent = 'Потрібно ввести однакові значення';
        errorMessage.style.color = 'red';

        const existingErrorMessage = document.querySelector('.error-message');
        if (existingErrorMessage) {
            existingErrorMessage.remove();
        }

        passwordInputs[1].parentNode.appendChild(errorMessage);
        errorMessage.classList.add('error-message');
    }
});
